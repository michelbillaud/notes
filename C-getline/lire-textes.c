// Démonstration de getline
// M Billaud, 2919

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void numeroter_lignes(const char *chemin);

int main(int argc, char * argv[])
{
    for (int i = 1; i < argc; i++) {
	numeroter_lignes(argv[i]);
    }
}

// ----------------------------------------

void numeroter_lignes(const char *chemin)
{
    static char *tampon = NULL;
    static size_t taille_tampon = 0;

    FILE *fichier  = fopen(chemin, "r");
    if (fichier == NULL) {
	fprintf(stderr, "* Fichier '%s' absent ou illisible.\n", chemin);
	return;
    }
    
    printf("*\n* %s\n*\n", chemin);
    
    int numero = 0;
    while(true) {
	ssize_t taille = getline(& tampon, & taille_tampon, fichier);
	if (taille < 0) {
	    break;
	}
	numero += 1;
	printf("%4d\t%s", numero, tampon); // le \n est dans le tampon
    }

    fclose(fichier);
    free(tampon);
}
