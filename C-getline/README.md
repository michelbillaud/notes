# Objectif

Ce programme montre une utilisation de `getline`,
disponible depuis POSIX.1-2008.

https://pubs.opengroup.org/onlinepubs/9699919799/functions/getdelim.html

Il prend en paramètre des noms de fichiers texte, et les 
affiche en numérotant les lignes.


# Pour compiler

- taper `make`
- ou utilisez votre compilateur favori. N'oubliez pas l'option
  nécessaire POSIX


# Pour exécuter : 

Exemple

~~~
$ ./lire-textes Makefile 
*
* Makefile
*
   1	# Options de compilation
   2	
   3	CFLAGS += -std=c11 -D_POSIX_C_SOURCE=200809L
   4	
   5	CFLAGS += -Wall -Wextra -pedantic
   ...
~~~


# Commentaires sur le source

La fonction `getline` alloue/réalloue un tampon pour y caser la ligne
qui est lue sur le fichier. Ce tampon est réalloué si il n'est pas
assez grand.

La fonction `numeroter_lignes` détient 
le pointeur sur le tampon et sa longueur. 

Elle appelle `getline` avec les **adresses** de ces variables, qui sont
modifiées par getline.


Ici on a choisi d'utiliser des variables **statiques** ; on réutilise donc le même tampon jusqu'à la fin du programme sans le libérer.

Une autre solution aurait été d'employer des variables automatiques, 
et de libérer le tampon en sortant de la fonction.

~~~
void numeroter_lignes(const char *chemin)
{
    char *tampon = NULL;              // non statiques
    size_t taille_tampon = 0;

    while(true) {
	   ...
    }
	
	fclose(fichier);
	free(tampon);                     // libération
}
~~~
